#include "my_vm.h"

static char physical_buffer[MEMSIZE];
static unsigned long num_offset_bits;
static unsigned long num_pt_bits;
static unsigned long num_pd_bits;
static char* valid_frame;
static char* valid_page; 

static page_table** pg_dir;
static long pg_dir_size;
static unsigned long num_pt_entries;

static int is_phys_mem_alloc = 0;
static int no_free_mem = 0;
static unsigned long v_mem_entries = MEMSIZE/PGSIZE;

void* get_next_avail();
static unsigned long log_2(unsigned long x);
static void bit_mask(unsigned long* a, unsigned long shift);
static void init_inner_table(unsigned long index);
static unsigned long ceil(double num);

static pthread_mutex_t mutexes[4];

static unsigned long log_2(unsigned long x){
	int ret = 0;
	while(x >>= 1) ret++;
	return ret;
}

static unsigned long ceil(double num){
	return (num - ((unsigned long) num)) ? (unsigned long)(num + 1) : (unsigned long) num;
}

static void bit_mask(unsigned long* a, unsigned long shift){
	unsigned long mask = (1<<shift);
	*a &= (mask-1);
}

static void init_inner_table(unsigned long index){
	pg_dir[index] = (page_table*) malloc(sizeof(page_table));
	pg_dir[index]->frame = (long*) malloc(sizeof(long)*(num_pt_entries));
	memset(pg_dir[index]->frame, -1, sizeof(long*) * num_pt_entries);
}

void set_physical_mem() {

	int i;
	for(i=0; i<TLB_ENTRIES; i++){
		memset(&tlb_store[i], 0, sizeof(struct tlb));
	}

	unsigned long va_space = log_2(MEMSIZE);	//virtual address space
	num_offset_bits = log_2(PGSIZE);		//num_offset_bits bits
	num_pt_bits = num_offset_bits-log_2((unsigned long) sizeof(pte_t));			//page table bits. assumes pte size of 4 bytes
	num_pd_bits = va_space - num_offset_bits - num_pt_bits;	//page dir bits
	pg_dir_size = 1<<num_pd_bits;
	num_pt_entries = 1<<num_pt_bits;

	valid_frame = (char*) malloc(sizeof(char) * (v_mem_entries));
	memset(valid_frame, 'y', v_mem_entries);
	valid_page = (char*) malloc(sizeof(char) * (v_mem_entries));
	memset(valid_page, 'y', v_mem_entries);
	pg_dir = (page_table**) malloc(sizeof(page_table*)*pg_dir_size);
	memset(pg_dir, 0, sizeof(page_table*)*pg_dir_size);

	for(i=0; i<4; i++){
		pthread_mutex_init(&mutexes[i], NULL);
	}
}

static unsigned long hash(unsigned long va){
	return va%TLB_ENTRIES;
}

int
add_TLB(void *va, void *pa)
{

    /*Part 2 HINT: Add a virtual to physical page translation to the TLB */
	unsigned long index = hash((unsigned long) va);
	tlb_store[index].va_pa[0] = (unsigned long) va;
	tlb_store[index].va_pa[1] = (unsigned long) pa;

    return 0;
}

pte_t *
check_TLB(void *va) {

    /* Part 2: TLB lookup code here */
	unsigned long index = hash((unsigned long) va);
	if(tlb_store[index].va_pa[0] == (unsigned long) va){
		hits++;
		return (pte_t*) tlb_store[index].va_pa[1];
	}
	misses++;
	return NULL;
}

void
print_TLB_missrate()
{
    double miss_rate = misses/(misses+hits);	
    fprintf(stderr, "TLB miss rate %lf \n", miss_rate);
}

static unsigned long get_pde_index(unsigned long va){
	return va >> (num_offset_bits+num_pt_bits);
}

static unsigned long get_pte_index(unsigned long va){
	unsigned long pte_index = va >> num_offset_bits;
	bit_mask(&pte_index, num_pt_bits);
	return pte_index;
}

static unsigned long get_offset(unsigned long va){
	bit_mask(&va, num_offset_bits);
	return va;
}

pte_t *translate(void *va) {
	//check TLB
    pte_t* pap;
	if((pap = check_TLB(va)) != NULL) return pap;
	
	//translate
	unsigned long pde_index;
	if(pg_dir[(pde_index = get_pde_index((unsigned long) va))] == NULL) return NULL;
	
	unsigned long pte_index = get_pte_index((unsigned long) va);
	unsigned long pa;
	if((pa = pg_dir[pde_index]->frame[pte_index]) < 0) return NULL;

	pa <<= num_offset_bits;
	pa |= get_offset((unsigned long) va);
	
	add_TLB(va, (void*)pa);
	return (unsigned long*) pa; 
}

static long get_frame(unsigned long pa){
	return pa >> num_offset_bits;
}

//return 0 on exits, 1 on created
int
page_map(void *va, void *pa)
{
	
	unsigned long pde_index;
	if(pg_dir[(pde_index = get_pde_index((unsigned long) va))] == NULL){
		init_inner_table(pde_index);
	}

	unsigned long pte_index = get_pte_index((unsigned long) va);
	if(pg_dir[pde_index]->frame[pte_index] < 0){
		pg_dir[pde_index]->frame[pte_index] = get_frame((unsigned long) pa);
		return 0;
	}
	
	return 1;
}

//This literally scans for the first region of physical memory that isnt allocated
void* get_next_avail(){
	unsigned long i;
	for(i = 0; i < v_mem_entries; i++){
		if(valid_frame[i] == 'y') {
			valid_frame[i] = 'n';
			return (void*)(physical_buffer + (i*PGSIZE)); // This is the beginning address of each frame in physical mem, each index in valid frame spans PGSIZE bytes
		}
	}
}

//Looks for a region of contiguous memory by checking the valid page bitmap
static unsigned long search_v_mem(int num_pages) {
	unsigned long i;
	unsigned long cont_mem_count;

	for(i = 0, cont_mem_count = 0; i < v_mem_entries; i++){
		if(valid_page[i] == 'y'){
			cont_mem_count++;
			if(cont_mem_count  == num_pages){
				no_free_mem = 0;
				return i - (num_pages - 1);//This shifts the index back to where it started
			}
		} else{
			cont_mem_count = 0;
		}
	}
	no_free_mem = 1; //Flag for cases where no contiguous memory is found, I used it because this needs to return an unsigned long, and as such I cant use -1
	return 0;
}

void *a_malloc(unsigned int num_bytes) {
 
	pthread_mutex_lock(&mutexes[0]);

	if(!is_phys_mem_alloc){
		set_physical_mem();
		is_phys_mem_alloc = 1;
	}
   	int num_of_pages = (int)ceil((double)num_bytes/ (double)PGSIZE);

	unsigned long map_index = search_v_mem(num_of_pages);

    if(no_free_mem){
		pthread_mutex_unlock(&mutexes[0]);
	   return NULL;
   	}
	
   	unsigned long pde_index = (unsigned long) (map_index / num_pt_entries);

	if(pg_dir[pde_index] == NULL){
		init_inner_table(pde_index);
	}

	unsigned long i;
	unsigned long pte_index;
	unsigned long va_offset = 0;
	//This block of code maps the va to pa, and configures the virtual bitmap for use
	for(i = map_index; i < map_index + num_of_pages; i++){
		pte_index = i - ((unsigned long)(i/num_pt_entries) * num_pt_entries);
		pde_index = (unsigned long)(i/num_pt_entries);


		valid_page[i] = 'n';

		pde_index <<= num_offset_bits + num_pt_bits;
		pte_index <<= num_offset_bits;

		void* va = (void*)(pde_index | pte_index);
		void* pa = get_next_avail();
		va_offset = (unsigned long) pa;
		bit_mask(&va_offset, num_offset_bits);
		va = (void*) ((unsigned long) va | va_offset);

		page_map(va, pa);
	}
	pde_index = (unsigned long) map_index / num_pt_entries;
	map_index <<= num_offset_bits;
	pde_index <<= (num_offset_bits + num_pt_bits);

	pthread_mutex_unlock(&mutexes[0]);

    return (void *) (pde_index | map_index | va_offset);
}

/* Responsible for releasing one or more memory pages using virtual address (va)
*/
void a_free(void *va, int size) {
	
	pthread_mutex_lock(&mutexes[1]);

	unsigned long index = hash((unsigned long) va);
	if(tlb_store[index].va_pa[0] == (unsigned long) va){
		memset(&tlb_store[index], 0, sizeof(struct tlb));
	}

	int num_of_pages = (int)ceil((double)size / (double)PGSIZE);

	//checks to see if the address it was given even contains the proper number of used pages
	unsigned long map_index = get_pte_index((unsigned long) va) + (get_pde_index((unsigned long) va) * num_pt_entries);
	unsigned long i;
    for(i = map_index; i < map_index + num_of_pages; i++){
		if(valid_page[i] == 'y'){
			perror("Cannot free, already freed or unused memory");
			pthread_mutex_unlock(&mutexes[1]);
			return;
		}
	}

	//This is where stuff actually is freed
	unsigned long pde_index;
	unsigned long pte_index;
	for (i = map_index; i < map_index + num_of_pages; i++) {
		pte_index = (i - ((unsigned long)(i/num_pt_entries) * num_pt_entries));
		pde_index = (unsigned long)(i/num_pt_entries);
		
		valid_page[i] = 'y';
		valid_frame[(pg_dir[pde_index]->frame[pte_index])/PGSIZE] = 'y';
		pg_dir[pde_index]->frame[pte_index] = -1;
	}
	
	pthread_mutex_unlock(&mutexes[1]);
}


void put_value(void *va, void *val, int size) {

	pthread_mutex_lock(&mutexes[2]);
	
	unsigned long num_pages = ceil(((double)size)/((double)PGSIZE));
	unsigned long padded_bytes = num_pages*PGSIZE;

	unsigned char val_segment[padded_bytes];
	memset(val_segment, 0, sizeof(val_segment));
	memcpy(val_segment, val, padded_bytes);
	int k = 0;	//keep track of what segment to copy next;

	int i;
	for(i=0; i < num_pages || size != 0; i++){
		
		void* pa = (void*) translate(va);
	
		if(size > PGSIZE){

			memcpy(pa, val_segment+k, PGSIZE);
			size -= PGSIZE;
			k += PGSIZE;
			va = (void*)((unsigned long) va + PGSIZE);	//hopefully this works
		}else{
			memcpy(pa, val_segment+k, size);
			size -= size;
		}
	}
	pthread_mutex_unlock(&mutexes[2]);
}


/*Given a virtual address, this function copies the contents of the page to val*/
void get_value(void *va, void *val, int size) {

	pthread_mutex_lock(&mutexes[3]);

	unsigned long num_pages = ceil(((double)size)/((double)PGSIZE));
	int k = 0;

	int i;
	for(i=0; i < num_pages && size != 0; i++){
		
		char* pa = (char*) translate(va);
		
		if(size > PGSIZE){
			
			memcpy(val+k, pa, PGSIZE);
			k += PGSIZE;
			size -= PGSIZE;
			va = (void*)((unsigned long) va + PGSIZE);
		}else{	
			memcpy(val+k, pa, size);
			size-=size;
		}
	}

	pthread_mutex_unlock(&mutexes[3]);
}

void mat_mult(void *mat1, void *mat2, int size, void *answer) {
  
	  int i;
      int j;
      int k;
      int sum;
      int mat_elm;
      int mat_elm2;
      int addr_mat1;
      int addr_mat2;
      int addr_answer;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            sum = 0;
            for(k = 0; k < size; k++){
                addr_mat1 = (unsigned int)mat1 + ((k * size * sizeof(int))) + (j * sizeof(int));
                addr_mat2 = (unsigned int)mat2 + ((k * size * sizeof(int))) + (j * sizeof(int));

                get_value((void *)addr_mat1, &mat_elm, sizeof(int));
                get_value((void *)addr_mat2, &mat_elm2, sizeof(int));

                sum += mat_elm * mat_elm2;
            }
         put_value((void *)((unsigned int)answer + ((i * size * sizeof(int))) + (j * sizeof(int))),&sum,sizeof(int));
        }
    } 

}
