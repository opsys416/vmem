#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "../my_vm.h"

#define ARRAY_SIZE 400

void gen_matrices(int mat1, void* va1, int val1, int SIZE){
    int i;
    int j;
    for (i = 0; i < SIZE; i++) {
            for (j = 0; j < SIZE; j++) {
                mat1 = (unsigned int)va1 + ((i * SIZE * sizeof(int))) + (j * sizeof(int));
                put_value((void *)mat1, &val1, sizeof(int));
            }
        } 
}

void print_matrix(int mat1, void* va1, int val1, int SIZE){
    int i;
    int j;
    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE; j++) {
            mat1 = (unsigned int)va1 + ((i * SIZE * sizeof(int))) + (j * sizeof(int));
            get_value((void *)mat1, &val1, sizeof(int));
            printf("%d ", val1);
        }
        printf("\n");
    }
    printf("\n");
}



int main() {

    //printf("Allocating three arrays of %d bytes\n", ARRAY_SIZE);

    void *a = a_malloc(ARRAY_SIZE);
    int old_a = (int)a;
    void *b = a_malloc(ARRAY_SIZE);
    void *c = a_malloc(ARRAY_SIZE);
    int val1 = 10;
    int val2 = 12;
    int val3;
    int y, z;
    int i =0, j=0;
    int address_a = 0, address_b = 0;
    int address_c = 0;
    int SIZE = 5;

    unsigned long rand_arr[15000];
    memset(rand_arr,-1,sizeof(rand_arr));
    //printf("Addresses of the allocations: %x, %x, %x\n", (int)a, (int)b, (int)c);

    srand(time(0));
   for(i = 0; i < 10000; i++){
         a = a_malloc(ARRAY_SIZE);
         b = a_malloc(ARRAY_SIZE);
         c = a_malloc(ARRAY_SIZE);
         gen_matrices(address_a, a, val1, SIZE);
         gen_matrices(address_b, b, val2, SIZE);
        
        

        mat_mult(a, b, SIZE, c);

        if(i == 0) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }

         if(i == 1000) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }

        if(i == 2000) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }
        if(i == 3000) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }

        if(i == 4000) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }
        if(i == 8000) {
            print_matrix(address_a, a, val1, SIZE);
            print_matrix(address_b, b, val2, SIZE);
            print_matrix(address_c, c, val3, SIZE);
        }
        rand_arr[i] = (unsigned long) a;
        rand_arr[i+1] = (unsigned long)b;
        rand_arr[i+2] = (unsigned long)c;
        int k = rand() % (15000-1+ 1);
        if(rand_arr[k] != -1){
            a_free((void *) rand_arr[k],i);
        }
   }
    /*for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE; j++) {
            address_c = (unsigned int)c + ((i * SIZE * sizeof(int))) + (j * sizeof(int));
            get_value((void *)address_c, &y, sizeof(int));
            printf("%d ", y);
        }
        printf("\n");
    }
    printf("Freeing the allocations!\n");
    a_free(a, ARRAY_SIZE);
    a_free(b, ARRAY_SIZE);
    a_free(c, ARRAY_SIZE);

    printf("Checking if allocations were freed!\n");
    a = a_malloc(ARRAY_SIZE);
    if ((int)a == old_a)
        printf("free function works\n");
    else
        printf("free function does not work\n");

    
    print_TLB_missrate();*/
    return 0;
}
